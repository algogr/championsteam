package gr.algo.championsteam

import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_communication.*
import okhttp3.*
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference

class CommunicateTask(val context:CommunicationActivity) : AsyncTask<Int, Int, String?>() {
    val mContext = context
    private var mResponse = 0
    private var result: String? = null
    val handler = MyDBHandler(context = mContext, version = 1, name = null, factory = null)
    //private val activityReference: WeakReference<UploadDB> = WeakReference(context)
    //private val address=handler.getSettings(1)
    //private val port=handler.getSettings(2)

    private val activityReference: WeakReference<CommunicationActivity> = WeakReference(context)
    private val address = handler.getSettings(3)
    private val port = handler.getSettings(4)



    override fun doInBackground(vararg params: Int?): String? {
        val MEDIA_TYPE_PNG = MediaType.parse("binary")
        val client = OkHttpClient()
        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("title", "test123")
            .addFormDataPart(
                "uploadfile", "algo.sqlite", RequestBody.create(
                    MEDIA_TYPE_PNG,
                    File("/data/data/gr.algo.algomobilemini/databases/algo.sqlite")
                )
            )
            .build()


        val serverUrl = "http://" + address + ":" + port + "/upload"
        val request = Request.Builder()
            .url(serverUrl)
            .post(requestBody)
            .build()


        val response = client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

                result = "Υπάρχει πρόβλημα στη σύνδεση με τον server"

                onPostExecute("Υπάρχει πρόβλημα στη σύνδεση με τον server")

            }

            override fun onResponse(call: Call, response: Response) {
                if (response.code() == 200) {
                    // result = "Η αποστολή ήταν επιτυχής"

                    onPostExecute("Η αποστολή ήταν επιτυχής")
                } else {
                    onPostExecute("Αποτυχία αποστολής!!")
                }

                mResponse = 1
            }


        })


        /*

        if (!response.isSuccessful) {
            result="Υπάρχει πρόβλημα στη σύνδεση με τον server"

        } else {
            result="Η αποστολή ήταν επιτυχής"

        }

         */

        return result
    }

    protected fun onProgressUpdate(vararg progress: Int) {
    }

    val h: Handler = Handler(Looper.getMainLooper());


    protected override fun onPostExecute(result: String?) {
        /*
        val activity = activityReference.get()
        if (activity == null || activity.isFinishing) return
        //activity.progressBar.visibility = View.GONE
        //activity.toastMsg=result.let{ it }
        Toast.makeText(activity,result.let{it}, Toast.LENGTH_LONG).show()

         */
        if (result != null) {
            h.post(Runnable() {
                run() {
                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                    if (mResponse == 1) {
                        mContext.button.isEnabled = true

                    }
                }
            })
        }


    }
}
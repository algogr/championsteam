package gr.algo.championsteam

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_communication.*

class CommunicationActivity : AppCompatActivity() {
    lateinit var toastMsg:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_communication)
        button.setOnClickListener {
            toastMsg="Η αποστολή ξεκίνησε"
            if (isNetworkAvailable()) {
                button.isEnabled=false

                CommunicateTask(this).execute(1)

                val handler=MyDBHandler(context = this.baseContext,version = 1,name=null,factory = null)

                Toast.makeText(this,toastMsg, Toast.LENGTH_LONG).show()

            }

            else
                Toast.makeText(baseContext,"Δεν υπάρχει δίκτυο", Toast.LENGTH_LONG).show()


        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i= Intent(this,MainActivity::class.java)

        startActivity(i)
    }


}

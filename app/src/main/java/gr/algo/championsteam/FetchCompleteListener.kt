package gr.algo.championsteam

interface FetchCompleteListener {
    fun fetchComplete()
}
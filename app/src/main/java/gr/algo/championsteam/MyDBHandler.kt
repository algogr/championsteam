package gr.algo.championsteam

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Bundle
import android.util.Log
import java.io.Serializable
import java.time.LocalDate
import java.time.format.DateTimeFormatter


 class MyDBHandler(context: Context,name:String?,factory:SQLiteDatabase.CursorFactory?,
                        version: Int):SQLiteOpenHelper(context,DATABASE_NAME,factory,DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        createTables(db)


    }


    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

    }


    fun createTables(db: SQLiteDatabase) {
        val CREATE_TICKETS: String =
            "CREATE TABLE IF NOT EXISTS Tickets (id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,name	TEXT,address	TEXT,district	TEXT,city	TEXT" +
                    ",postalcode	TEXT,tel1	TEXT,tel2	TEXT,tel3	TEXT,malfunctiontype	TEXT,comments	TEXT,centercode	TEXT,priority	TEXT,tickettype	TEXT,visitdatetime	TEXT," +
                    "countydescr	TEXT,erpid INTEGER,status TEXT, signfilename TEXT, photofilename TEXT, starttime TEXT,endtime TEXT,unittype TEXT,servicetype TEXT,category TEXT,payment INTEGER,erpupd INTEGER, reportfilename TEXT)"

        val CREATE_TABLETINFO:String="CREATE TABLE IF NOT EXISTS TABLETINFO (techid TEXT,techpassword TEXT,serveraddress TEXT,serverport INTEGER)"
        val INSERT_DEMO_SETTINGS="INSERT INTO TABLETINFO (techid,techpassword,serveraddress,serverport) SELECT 'ΞΙΜ','123','192.168.1.254',8080 WHERE NOT EXISTS (SELECT * FROM TABLETINFO)"

        db.execSQL(CREATE_TICKETS)
        db.execSQL(CREATE_TABLETINFO)
        db.execSQL(INSERT_DEMO_SETTINGS)
    }

     fun insertTicket(ticket:Ticket)
     {
         val db=this.writableDatabase
         var query="SELECT * FROM tickets where erpid=${ticket.erpId}"
         val cursor=db.rawQuery(query,null)
         if (cursor.count==0)
         {
             query="INSERT INTO tickets (name,address,district,city,postalcode,tel1,tel2,malfunctiontype,comments,centercode,priority,tickettype,visitdatetime,countydescr,erpid,status,unittype,servicetype," +
                     "category,payment) " +
                     "VALUES ('${ticket.name}','${ticket.address}','${ticket.district}','${ticket.city}','${ticket.postalCode}','${ticket.phone1}','${ticket.phone2}','${ticket.malfunctionType}'," +
                     "'${ticket.comments}','${ticket.centerCode}','${ticket.priority}','${ticket.malfunctionType}','${ticket.visitDateTime}','${ticket.countyDescr}',${ticket.erpId},'${ticket.status}',+" +
                     "'${ticket.unitType}','${ticket.serviceType}','${ticket.category}',${ticket.payment})"
             Log.d("JIM-2",query)

             db.execSQL(query)

         }
         db.close()



     }


     fun updateTicket(ticket:Ticket){
         val db=this.writableDatabase
            val query="UPDATE tickets " +
                    "SET  comments='${ticket.comments}',signfilename='${ticket.signFileName}',photofilename='${ticket.photoFileName}',starttime='${ticket.startTime}',endtime='${ticket.endTime}'," +
                    "unittype='${ticket.unitType}',servicetype='${ticket.serviceType}',payment=${ticket.payment},category='${ticket.category}' where erpid=${ticket.erpId}"
         Log.d("JIM",query)
         db.execSQL(query)
         db.close()

     }

     fun updateTicketReport(ticket:Ticket){
         val db=this.writableDatabase
         val query="UPDATE tickets " +
                 "SET  reportfilename='${ticket.reportFileName}' where erpid=${ticket.erpId}"
         Log.d("JIM",query)
         db.execSQL(query)
         db.close()

     }

     fun getTickets():MutableList<Ticket>
     {
         val db=this.writableDatabase
         val query="SELECT name,ifnull(address,''),ifnull(district,''),ifnull(city,''),ifnull(postalcode,''),ifnull(tel1,''),ifnull(tel2,''),ifnull(malfunctiontype,''),ifnull(comments,''),ifnull(centercode,''),ifnull(priority,''),ifnull(tickettype,''),ifnull(visitdatetime,''),ifnull(countydescr,''),ifnull(erpid,0),ifnull(status,''),ifnull(tickettype,''),ifnull(signfilename,''),ifnull(photofilename,''),ifnull(starttime,'')," +
                 "ifnull(endtime,''),ifnull(unittype,''),ifnull(servicetype,''),ifnull(category,''),ifnull(payment,0),ifnull(erpupd,0),ifnull(reportfilename,'') from tickets where ((erpupd is null) or (erpupd=0)) order by visitdatetime desc"
         val cursor=db.rawQuery(query,null)
         val ticketList= mutableListOf<Ticket>()
         for (i in 0..cursor.count-1)
         {
             cursor.moveToPosition(i)
             val ticket=Ticket(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),
                 cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),
                 cursor.getString(10),cursor.getString(12),cursor.getString(13),cursor.getInt(14),0,cursor.getString(15),
                 cursor.getString(17),cursor.getString(18),cursor.getString(19),cursor.getString(20),cursor.getString(21),cursor.getString(22),cursor.getString(23),
                 cursor.getInt(24),cursor.getInt(25),cursor.getString(26))

             ticketList.add(ticket)

         }
         cursor.close()
         db.close()
         return ticketList
     }


     fun getTicketsforErpUpdate():MutableList<Ticket>
     {
         val db=this.writableDatabase
         val query="SELECT name,address,district,city,postalcode,tel1,tel2,malfunctiontype,comments,centercode,priority,tickettype,visitdatetime,countydescr,erpid,status,tickettype,signfilename,photofilename,starttime," +
                 "endtime,unittype,servicetype,category,payment,erpupd,reportfilename from tickets where ((endtime <> null) or (endtime<>'')) and ((erpupd=0) or (erpupd is null))"
         val cursor=db.rawQuery(query,null)
         val ticketList= mutableListOf<Ticket>()
         for (i in 0..cursor.count-1)
         {
             cursor.moveToPosition(i)
             val ticket=Ticket(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),
                 cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),
                 cursor.getString(10),cursor.getString(12),cursor.getString(13),cursor.getInt(14),0,cursor.getString(15),
                 cursor.getString(17),cursor.getString(18),cursor.getString(19),cursor.getString(20),cursor.getString(21),cursor.getString(22),cursor.getString(23),
                 cursor.getInt(24),cursor.getInt(25),cursor.getString(26))
             ticketList.add(ticket)

         }
         cursor.close()
         db.close()
         return ticketList
     }


     fun setUpdateFlag(ticket:Ticket){
         val db=this.writableDatabase
         val query="UPDATE tickets " +
                 "SET  erpupd=1 where erpid=${ticket.erpId}"
         db.execSQL(query)
         db.close()


     }


     fun getSettings(field:Int):String{
         val query="SELECT techid,techpassword,serveraddress,serverport  from tabletinfo"
         val db=this.writableDatabase
         val cursor:Cursor?=db.rawQuery(query,null)
         cursor?.moveToPosition(0)
         var answer:String?=""
         when (field){
             1->answer=cursor?.getString(0).toString()
             2->answer=cursor?.getString(1).toString()
             3->answer=cursor?.getString(2).toString()
             4->answer=cursor?.getInt(3).toString()

         }
         cursor?.close()
         db.close()

         return answer?:""
     }


     fun updateSettings(settings:List<String>)
     {
         val query="UPDATE tabletinfo SET techid='${settings[0]}',techpassword='${settings[1]}',serveraddress='${settings[2]}',serverport=${settings[3]}"

         val db=this.writableDatabase
         db.execSQL(query)

     }




     fun emptyDb(){
         val query="DELETE FROM Tickets"
         val db=this.writableDatabase
         db.execSQL(query)
         db.close()

     }


    companion object {
        private val DATABASE_VERSION=1
        // private val DATABASE_NAME="/storage/sdcard1/algomobilemini/algo.sqlite"
        private val DATABASE_NAME="champions.sqlite"
    }




}


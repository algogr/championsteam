package gr.algo.championsteam

import android.app.DownloadManager
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import com.google.gson.Gson

import kotlinx.android.synthetic.main.activity_send.*
import kotlinx.android.synthetic.main.content_send.*
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Environment
import android.util.Log
import android.view.View
import okhttp3.*
import java.io.File
import java.io.IOException
import java.lang.System.exit


class Send : AppCompatActivity() {


    //private var client= OkHttpClient()
    private val handler = MyDBHandler(context = this@Send, version = 1, name = null, factory = null)
    private val client = OkHttpClient.Builder()
        .authenticator(object : Authenticator {
            @Throws(IOException::class)
            override fun authenticate(route: Route?, response: Response): Request? {
                val credential = Credentials.basic(handler.getSettings(1), handler.getSettings(2))
                return response.request().newBuilder()
                    .header("Authorization", credential)
                    .build()
            }
        })
        .build()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send)
        setSupportActionBar(toolbar)


        button2.setOnClickListener() {
            button2.visibility= View.INVISIBLE

            val ticketList = handler.getTicketsforErpUpdate()


            val url = "http://${handler.getSettings(3)}:${handler.getSettings(4)}/api/v1/updateincidents"
            val updateList: String = Gson().toJson(ticketList)

            val body = RequestBody.create(
                MediaType.parse("application/json"), updateList
            )


            val request = Request.Builder()
                .method("POST", body)
                .url(url)
                .build()



            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")
                        if (response.isSuccessful)  sendSignatures(ticketList)


                    }
                }
            })

        }


            button3.setOnClickListener {
                val handler = MyDBHandler(context = this@Send, version = 1, name = null, factory = null)

                handler.emptyDb()

            }

            fab.setOnClickListener { view ->
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }


    fun sendSignatures(ticketList: MutableList<Ticket>){
        var i=0
        for (ticket in ticketList) {
            i++
            if (ticket.signFileName!=null) {
                val url = "http://${handler.getSettings(3)}:${handler.getSettings(4)}/api/v1/uploadsigns"
                val body = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "image", "SIGN_${ticket.signFileName!!.substringAfter("_")}",
                        RequestBody.create(MediaType.parse("image/png"), File(ticket.signFileName))
                    )
                    .build()


                val request = Request.Builder()
                    .method("POST", body)
                    .url(url)
                    .build()


                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            if (!response.isSuccessful) throw IOException("Unexpected code $response")

                            if (i == ticketList.size) {
                                if (response.isSuccessful) sendPhotos(ticketList)
                            }
                        }

                        //for ((name, value) in response.headers) {
                        //  println("$name: $value")
                        //}

                        //println(response.body!!.string())

                    }
                })


            }

        }

    }

    fun sendPhotos(ticketList:MutableList<Ticket>) {
        var i = 0
        for (ticket in ticketList) {
            i++
            if (ticket.photoFileName != null) {
                val url = "http://${handler.getSettings(3)}:${handler.getSettings(4)}/api/v1/uploadphotos"
                val body = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "image", "PHOTO_${ticket.photoFileName!!.substringAfter("_")}",
                        RequestBody.create(MediaType.parse("image/jpg"), File(ticket.photoFileName))
                    )
                    .build()


                val request = Request.Builder()
                    .method("POST", body)
                    .url(url)
                    .build()


                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            if (!response.isSuccessful) throw IOException("Unexpected code $response")
                            if (i == ticketList.size) {

                                if (response.isSuccessful) sendReports(ticketList)

                            }


                            //for ((name, value) in response.headers) {
                            //  println("$name: $value")
                            //}

                            //println(response.body!!.string())
                        }
                    }
                })


            }


        }
    }

    fun sendReports(ticketList:MutableList<Ticket>)
    {
        var i=0

        for (ticket in ticketList) {
            i++
            if(ticket.reportFileName!=null) {
                val url = "http://${handler.getSettings(3)}:${handler.getSettings(4)}/api/v1/uploadreports"
                Log.d("JIM_____REPOR",ticket.reportFileName)
                val body = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "report",ticket!!.reportFileName!!.takeLast(25),
                        RequestBody.create(
                            MediaType.parse("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
                            File(ticket.reportFileName)
                        )
                    )
                    .build()


                val request = Request.Builder()
                    .method("POST", body)
                    .url(url)
                    .build()


                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.d("JIM","FAILURE")
                        Log.d("JIM",ticket.reportFileName)
                        e.printStackTrace()
                        Log.d("JIM",e.printStackTrace().toString())
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            Log.d("JIM-RESPONESE",response.toString())
                            if (!response.isSuccessful) throw IOException("Unexpected code $response")
                            handler.setUpdateFlag(ticket)
                            Log.d("JIM","Respone succesfull!!!")

                            val i = Intent(this@Send, MainActivity::class.java)

                            startActivity(i)


                            //for ((name, value) in response.headers) {
                            //  println("$name: $value")
                            //}

                            //println(response.body!!.string())
                        }
                    }
                })

            }
        }

        }


    }









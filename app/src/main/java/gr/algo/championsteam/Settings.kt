package gr.algo.championsteam

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;

import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.content_settings.*

class Settings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        val handler=MyDBHandler(context = this@Settings,version = 1,name=null,factory = null)
        techIdEdit.setText(handler.getSettings(1))
        techPassEdit.setText(handler.getSettings(2))
        addressEdit.setText(handler.getSettings(3))
        portEdit.setText(handler.getSettings(4))

        acceptButton3.setOnClickListener{
            val settings= listOf<String>(techIdEdit.text.toString(),techPassEdit.text.toString(),addressEdit.text.toString(),portEdit.text.toString())
            handler.updateSettings(settings)
            val i= Intent(this,MainActivity::class.java)
            startActivity(i)

        }

        cancelButton3.setOnClickListener{
            val i= Intent(this,MainActivity::class.java)
            startActivity(i)
        }
    }

}

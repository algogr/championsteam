package gr.algo.championsteam

import android.Manifest.permission.READ_CONTACTS
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_signature.*
import kotlinx.android.synthetic.main.content_signature.*
import java.io.FileOutputStream
import java.io.IOException
import java.lang.System.exit
import java.sql.Date
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.jar.Manifest

class SignatureActivity  : AppCompatActivity(),SignatureView.OnSignedListener,OnSignedCaptureListener {
    private val onSignedListener=this
    lateinit var ticket: Ticket

    var strTimeStamp={
        val answer:String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
            //val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            answer =  current.format(formatter)


        } else {
            val date = java.util.Date()
            // val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
            val formatter = java.text.SimpleDateFormat("yyyyMMddHHmmss")
            answer = formatter.format(date)


        }
        answer
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature)
        setSupportActionBar(toolbar)

        val extras= intent.extras

        ticket=extras?.getSerializable("ticket") as Ticket

        Log.d("JIM-CREATE",ticket.toString())

        buttonCancel.setOnClickListener{
            TODO()
        }

        buttonClear.setOnClickListener{
            TODO()
        }





        buttonOk.setOnClickListener{



            val filename=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/SIGN_"+strTimeStamp()+".png"

            Log.d("JIM-Sign-path",filename)
            val bitmap= signatureView.getSignatureBitmap()
            try {
                FileOutputStream(filename).use({ out ->
                    bitmap.compress(Bitmap.CompressFormat.PNG, 35, out) // bmp is your Bitmap instance
                })
            } catch (e: IOException) {
              e.printStackTrace()
            }

            ticket.signFileName=filename
            Log.d("JIM-Sign-signFilename",ticket.signFileName.toString())
            //onSignedListener.onSignatureCaptured(signatureView.getSignatureBitmap(), filename)

            val i = Intent(this@SignatureActivity, TechVisitActivity::class.java)

            val bundle=Bundle()

            Log.d("JIM-Sign-Catebory",ticket.category.toString())
            bundle.putSerializable("ticket",ticket)

            i.putExtras(bundle)
            //setResult(1,i)
            startActivity(i)


        }


        //signatureView.setOnSignedListener(this)






        /*
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

         */
    }


    override fun onSignatureCaptured(bitmap: Bitmap, fileUri: String) {


          }

    override fun onStartSigning() {
        //TODO("Not yet implemented")
    }

    override fun onSigned() {
        //TODO("Not yet implemented")
    }

    override fun onClear() {
        buttonClear.isEnabled=false

    }


    override fun onResume() {
        super.onResume()
        val extras= intent.extras


        ticket = extras?.getSerializable("ticket") as Ticket

        Log.d("JIM-RESUME",ticket.toString())

    }


    fun checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                val t=ActivityCompat.requestPermissions(this,
                    arrayOf(WRITE_EXTERNAL_STORAGE),
                    0)



                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {

        }

    }



    

}

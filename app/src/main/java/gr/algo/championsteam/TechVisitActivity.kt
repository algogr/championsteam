package gr.algo.championsteam

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_tech_visit.*
import kotlinx.android.synthetic.main.content_tech_visit.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class TechVisitActivity : AppCompatActivity() {
    var ticket:Ticket=Ticket()
    lateinit var serviceTypes:Array<String>
    lateinit var categories:Array<String>

    private val PERMISSION_CODE = 1000
    private val IMAGE_CAPTURE_CODE = 1001
    private  val DOC_CAPTURE_CODE=1002

    private var photoToggle=0

    var vFilename: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_tech_visit)
        serviceTypes=resources.getStringArray(R.array.ServiceTypes)
        categories=resources.getStringArray(R.array.Categories)

        //val extras= intent.extras
        //ticket=extras?.getSerializable("ticket") as Ticket
        val serviceAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,serviceTypes)
        val categoryAdapters=ArrayAdapter(this,android.R.layout.simple_spinner_item,categories)
        serviceTypeSpinner.adapter=serviceAdapter
        categorySpinner.adapter=categoryAdapters
        ticket.photoFileName?:let { imageViewPhoto.visibility= View.VISIBLE}



        var ftrDate={
            val answer:String
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val current = LocalDateTime.now()
                //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss")
                val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
                answer =  current.format(formatter)


            } else {
                val date = Date()
                //val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
                val formatter = SimpleDateFormat("dd-MM-yyyy")
                answer = formatter.format(date)


            }
            answer
        }


        var ftrTime={
            val answer:String
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val current = LocalDateTime.now()
                //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss")
                val formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
                answer =  current.format(formatter)


            } else {
                val date = Date()
                //val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
                val formatter = SimpleDateFormat("HH:mm:ss")
                answer = formatter.format(date)


            }
            answer
        }

        dateText.setText(ftrDate())

        startButton.setOnClickListener{
            startButton.setText(ftrTime())
            ticket.startTime=startButton.text.toString()

        }




        endButton.setOnClickListener{
            endButton.setText(ftrTime())
            ticket.endTime=endButton.text.toString()
        }



        acceptButton2.setOnClickListener{
            ticket.unitType=editUnitType.text.toString()
            ticket.serviceType=(serviceTypeSpinner.selectedItemPosition+1).toString()
            ticket.category=(categorySpinner.selectedItemPosition+1).toString()
            ticket.comments=commentsEdit.text.toString()
            val payment:()->Int={
            var type:Int=-1
            if (payedRadio.isChecked) {type=1}
            else {type=2}
            type
        }
            ticket.payment=payment()

            val handler=MyDBHandler(context = this.baseContext,version = 1,name=null,factory = null)


            handler.updateTicket(ticket)

            val i = Intent(this@TechVisitActivity, TicketListActivity::class.java)
            startActivity(i)
        }


        cancelButton2.setOnClickListener {
            val i = Intent(this@TechVisitActivity, TicketListActivity::class.java)
            startActivity(i)
        }


        getSignButton.setOnClickListener{
            ticket.unitType=editUnitType.text.toString()
            ticket.category=(categorySpinner.selectedItemPosition+1).toString()
            ticket.serviceType=(serviceTypeSpinner.selectedItemPosition+1).toString()
            ticket.comments=commentsEdit.text.toString()
            val payment:()->Int={
                var type:Int=-1
                if (payedRadio.isChecked) {type=1}
                else {type=2}
                type
            }
            ticket.payment=payment()
            Log.d("JIMBEFORE SGGN",ticket.toString())


            val i = Intent(this@TechVisitActivity, SignatureActivity::class.java)
            val bundle=Bundle()
            bundle.putSerializable("ticket",ticket)
            Log.d("JIM-SRARTSIGNACTIVITY",ticket.toString())
            i.putExtras(bundle)
            startActivity(i)

        }



        getPhotoButton.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                openCamera()

            } else {
                Toast.makeText(this@TechVisitActivity,"Χρειάζεται μεγαλύτερη έκδοση Android",Toast.LENGTH_SHORT).show()

            }

        }


        getDocButton.setOnClickListener{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                openWordDoc()
            } else {
                Toast.makeText(this@TechVisitActivity,"Χρειάζεται μεγαλύτερη έκδοση Android",Toast.LENGTH_SHORT).show()
            }
        }



        onResume()
        setSupportActionBar(toolbar)


    }





    override fun onResume() {
        super.onResume()




        val extras= intent.extras
        if (extras?.getSerializable("ticket")!=null) {


            ticket = extras?.getSerializable("ticket") as Ticket
            ticket.photoFileName?:let { imageViewPhoto.visibility=View.VISIBLE} /////////CHECK

            var ftrDate={
                val answer:String
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val current = LocalDateTime.now()
                    //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss")
                    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
                    answer =  current.format(formatter)


                } else {
                    val date = Date()
                    //val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
                    val formatter = SimpleDateFormat("dd-MM-yyyy")
                    answer = formatter.format(date)


                }
                answer
            }



            dateText.setText(ticket.visitDateTime?:ftrDate())
            startButton.setText(if(ticket.startTime.toString().isEmpty())"         ΕΝΑΡΞΗ         " else ticket.startTime.toString() )
            endButton.setText(if(ticket.endTime.toString().isEmpty()) "           ΛΗΞΗ            " else ticket.endTime.toString())
            editUnitType.setText(ticket.unitType)

            serviceTypeSpinner.post{serviceTypeSpinner.setSelection(((ticket.serviceType)!!.toInt())-1)}
            categorySpinner.post{categorySpinner.setSelection((ticket.category!!.toInt())-1)}

            commentsEdit.text=SpannableStringBuilder(ticket.comments.toString())
            if (ticket.payment==1)
                payedRadio.isChecked=true
            else
                debitRadio.isChecked=true
            val signFileName=ticket.signFileName
            if (signFileName!=null)
            {
                val imageFile=File(signFileName)

                val bitmap=BitmapFactory.decodeFile(imageFile.absolutePath)
                imageView.setImageBitmap(bitmap)

            }


                val photoFileName = ticket.photoFileName
                if (photoFileName != null) {
                    val imageFile = File(photoFileName)

                    //val bitmap=BitmapFactory.decodeFile(imageFile.absolutePath)
                    val bitmap = BitmapFactory.decodeFile(ticket.photoFileName)
                    imageViewPhoto.setImageBitmap(bitmap)

                }

            val reporFileName=ticket.reportFileName



            Log.d("JIM-RSUME",ticket.toString())










        }


        Log.d("JIM-RSUME",ticket.toString())




    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")

        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        // set filename
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())


        vFilename = "PHOTO_" + timeStamp + ".jpg"



        val tmp=File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/")

         if (!tmp.exists()){

             tmp.mkdirs()


         }


        // set direcory folder
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/", vFilename);
        //val file = File("/storage/sdcard0/Download/", vFilename);
        val image_uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);
        //val image_uri = FileProvider.getUriForFile(this, "gr.algo.championsteam", file);

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                50); }
            Log.d("JIMBEFORE PHOTO",ticket.toString())
            startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)

    }




    private fun openWordDoc() {









        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        vFilename = "form-$timeStamp.docx"




        //val image_uri = FileProvider.getUriForFile(this, "gr.algo.championsteam", file);


        val file=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/form.docx"
        // set direcory folder
        val fileNew = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/"+ vFilename


        File(file).copyTo(File(fileNew))


        Log.d("JIM-file",file)

        //val file = "/storage/sdcard0/Download/"+ vFilename

        //File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/form.docx").copyTo(File(file))


        ticket.reportFileName=fileNew

        val handler=MyDBHandler(context = this.baseContext,version = 1,name=null,factory = null)
        handler.updateTicketReport(ticket)

        Log.d("JIM-TICKWET-1",ticket.toString())

        //val uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", File("/storage/sdcard0/Download/form.docx").copyTo(File(file)));
        val uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider",File(fileNew));
        val intent = Intent(Intent.ACTION_VIEW)

        intent.apply {
            type="application/msword"
            data=(uri)
            flags=(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            putExtra(DocumentsContract.EXTRA_INITIAL_URI,uri)
        }


        //intent.setDataAndType(uri, "application/msword");
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.d("JIMBEFORE /DOCUMENT",ticket.toString())
        startActivityForResult(intent, DOC_CAPTURE_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            //File object of camera image
            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/", vFilename)
            Toast.makeText(this@TechVisitActivity,file.toString(),Toast.LENGTH_SHORT).show()
            //longToast(file.toString())

            //Uri of camera image
            val uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file)
            imageViewPhoto.setImageURI(uri)
            ticket.photoFileName=file.absolutePath
            photoToggle=1





            val i = Intent(this@TechVisitActivity, TechVisitActivity::class.java)

            val bundle=Bundle()
            bundle.putSerializable("ticket",ticket)

            i.putExtras(bundle)

            startActivity(i)



        }
    }







    override fun onBackPressed() {
        super.onBackPressed()
        val i= Intent(this,TicketListActivity::class.java)

        startActivity(i)
    }






}



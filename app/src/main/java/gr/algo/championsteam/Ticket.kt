package gr.algo.championsteam

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Ticket (@SerializedName("name")var name:String="", @SerializedName("address")var address:String="", @SerializedName("district") var district:String?="",
                     @SerializedName("city")var city:String="", @SerializedName("postalCode")var postalCode:String?="",
                     @SerializedName("phone1")var phone1:String="", @SerializedName("phone2")var phone2:String?="", @SerializedName("malfunctionType")var malfunctionType:String="",
                     @SerializedName("comments")var comments:String?="", @SerializedName("centerCode")var centerCode:String?="",
                     @SerializedName("priority")var priority:String="", @SerializedName("visitDateTime")var visitDateTime:String="", @SerializedName("countyDescr")var countyDescr:String="",
                     @SerializedName("id") var erpId:Int=-1,  var isUpd:Int=0, @SerializedName("status")var status:String?=null,@SerializedName("signfilename")var signFileName:String?=null,
                   @SerializedName("photofilename")var photoFileName:String?=null,@SerializedName("starttime")var startTime:String?=null,@SerializedName("endtime")var endTime:String?=null,@SerializedName("unittype")var unitType:String?=null,
                   @SerializedName("servicetype")var serviceType:String?=null,@SerializedName("category")var category:String?=null,@SerializedName("payment")var payment:Int=0,@SerializedName("erpupd")var erpupd:Int=0,
                   @SerializedName("reportfilename")var reportFileName:String?=null):Serializable

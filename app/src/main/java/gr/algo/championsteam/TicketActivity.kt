package gr.algo.championsteam

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log

import kotlinx.android.synthetic.main.activity_ticket.*
import kotlinx.android.synthetic.main.content_ticket.*

class TicketActivity : AppCompatActivity() {

    lateinit var ticket: Ticket
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->

            val bundle=Bundle()
            //bundle.putSerializable("ticket",ticket)
            val i = Intent(this@TicketActivity, TechVisitActivity::class.java)
            bundle.putSerializable("ticket",ticket)



            i.putExtras(bundle)
            this@TicketActivity.startActivity(i)

        }

        val extras= intent.extras

        ticket=extras.getSerializable("ticket") as Ticket

        nameText.text=ticket.name
        addressText.text=ticket.address
        districtText.text=ticket.district
        cityText.text=ticket.city
        postalcodetext.text=ticket.postalCode
        tel1text.text=ticket.phone1
        tel2text.text=ticket.phone2
        malfunctiotext.text=ticket.malfunctionType
        commentstext.text=ticket.comments
        centercodetext.text=ticket.centerCode
        prioritytext.text=ticket.priority
        visitdatetimeText.text=ticket.visitDateTime
        countytext.text=ticket.countyDescr
        statustext.text=ticket.status




    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i= Intent(this,TicketListActivity::class.java)

        startActivity(i)
    }
}

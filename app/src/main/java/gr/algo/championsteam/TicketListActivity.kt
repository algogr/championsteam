package gr.algo.championsteam

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.gson.Gson

import kotlinx.android.synthetic.main.activity_ticket_list.*
import kotlinx.android.synthetic.main.content_ticket_list.*
import okhttp3.OkHttpClient.Builder
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.Arrays.asList
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.ListAdapter
import okhttp3.*
import org.w3c.dom.Text
import java.util.*
import kotlin.collections.HashMap


class TicketListActivity : AppCompatActivity() {

    val handler=MyDBHandler(context = this@TicketListActivity,version = 1,name=null,factory = null)
    private val client = OkHttpClient.Builder()
        .authenticator(object : Authenticator {
            @Throws(IOException::class)
            override fun authenticate(route: Route?, response: Response): Request? {
                val credential = Credentials.basic(handler.getSettings(1), handler.getSettings(2))
                return response.request().newBuilder()
                    .header("Authorization", credential)
                    .build()
            }
        })
        .build()



    //private var client = OkHttpClient.Builder()
    //    .connectionSpecs(Arrays.asList(ConnectionSpec.COMPATIBLE_TLS))
      //  .build()

    private var request = OkHttpRequest(this.client)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_list)
        setSupportActionBar(toolbar)
        setTitle("Tickets")

        requestNew()

        val ticketList:MutableList<Ticket>? = handler.getTickets()
        val ticketListAdapter=TicketListViewAdapter(this@TicketListActivity,ticketList)
        ticketListView.adapter=ticketListAdapter






        fab.setOnClickListener{
            requestNew()

        }









    }



    fun requestNew():MutableList<Ticket>
    {

        val handler=MyDBHandler(context = this@TicketListActivity,version = 1,name=null,factory = null)
        val url = "http://${handler.getSettings(3)}:${handler.getSettings(4)}/api/v1/incident?user=${handler.getSettings(1)}"
        val ticketList= mutableListOf<Ticket>()
        request.GET(url, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()

                runOnUiThread{
                    try {
                        //val jObject=JSONObject(responseData)
                        //val jArray=jObject.getJSONArray("")
                        val jArray=JSONArray(responseData)

                        for (i in 0..jArray.length()-1){
                            val gson= Gson()
                            val ticket=gson.fromJson(jArray.getJSONObject(i).toString(),Ticket::class.java)
                            ticketList.add(ticket)
                            handler.insertTicket(ticket)


                        }


                        val ticketList:MutableList<Ticket>? = handler.getTickets()
                        val ticketListAdapter=TicketListViewAdapter(this@TicketListActivity,ticketList)
                        ticketListView.adapter=ticketListAdapter






                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }


            }

            override fun onFailure(call: Call?, e: IOException?) {

            }
        })

        return ticketList
    }




    inner class TicketListViewAdapter: BaseAdapter {

        private var ticketList:MutableList<Ticket>?
        private  var context: Context? =null
        private  val mInflator: LayoutInflater
        private var hashMapTexts:  HashMap<String,String>



        constructor(context: Context, ticketlist: MutableList<Ticket>?) : super(){
            this.ticketList=ticketlist
            this.context=context
            this.hashMapTexts= HashMap()
            this.mInflator= LayoutInflater.from(context)
            for ( ticket in ticketList!!) {

                this.hashMapTexts.put(ticket.address,"")

            }
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            val holder: TicketListActivity.ViewHolder
            val view: View

            if (convertView==null){
                view= layoutInflater.inflate(R.layout.ticketitem,parent,false)
                holder=ViewHolder(view)
                view.tag=holder

            }
            else {

                view = convertView
                holder = view.tag as ViewHolder
            }



            val color = {if(ticketList!![position].startTime!!.toString().isEmpty()) Color.BLUE
                else{ if(ticketList!![position].endTime!!.toString().isEmpty()) Color.RED else Color.GREEN}}

            holder.position=position
            holder.visitTypeText.text=ticketList!![position].malfunctionType
            holder.datetimeText.text=ticketList!![position].visitDateTime
            holder.districtText.text=ticketList!![position].district
            holder.cityText.text=ticketList!![position].city
            holder.ticketId=ticketList!![position].erpId
            holder.buttonText.setBackgroundColor(color())
            holder.ticket=ticketList!![position]
            return view
        }

        override fun getItem(position: Int): Any {
            return ticketList!![position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return ticketList!!.size
        }




    }


    private class ViewHolder(view: View?){
        var visitTypeText: TextView
        var datetimeText: TextView
        var districtText: TextView
        var cityText: TextView
        var buttonText:TextView
        var ticket:Ticket?=null


        var position:Int
        var ticketId=-1

        init {
            this.visitTypeText = view?.findViewById<TextView>(R.id.visitTypeText) as TextView
            this.datetimeText=view?.findViewById<TextView>(R.id.datetimeText)as TextView
            this.districtText=view?.findViewById<TextView>(R.id.districtText) as TextView
            this.cityText=view?.findViewById<TextView>(R.id.cityTextView) as TextView
            this.buttonText=view?.findViewById<TextView>(R.id.button) as TextView
            this.position = -1

            view.setOnClickListener { v: View ->



                val i = Intent(v.context, TicketActivity::class.java)
                val bundle=Bundle()
                bundle.putSerializable("ticket",ticket)

                i.putExtras(bundle)
                v.context.startActivity(i)
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i= Intent(this,MainActivity::class.java)

        startActivity(i)
    }




}
